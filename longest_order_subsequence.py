import numpy


def longest_ordered_subsequence(L):
    n = len(L)
    dp = numpy.zeros(shape=(1000, 1))
    for i in range(n):
        for j in range(i):
            if L[i] > L[j]:
                dp[i] = max(dp[i], dp[j] + 1);
    ans = 0
    for i in range(n):
        ans = max(ans, dp[i])
    return ans + 1


print(longest_ordered_subsequence([10, 22, 9, 33, 21, 50, 41, 60]))
if longest_ordered_subsequence([1, 7, 3, 5, 9, 4, 8]) == 4:
    print('passed')
else:
    print('failed')