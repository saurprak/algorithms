import numpy


def count_ponds(G):
    m = len(G)
    n = len(G[0])
    add = 0
    dp = numpy.empty([1000, 1000], dtype="str")
    x = numpy.empty([m, 1], dtype="str")
    for i in range(m):
        x = list(G[i])
        for j in range(len(x)):
            dp[i][j] = x[j]

    for j in range(m):
        for k in range(n):
            if dp[j][k] == '#':
                add = add + 1
                dfs(j, k, dp)
    print(add)
    return add


def dfs(x, y, dp):
    if dp[x][y] == '#':
        dp[x][y] = '-'
        dfs(x - 1, y - 1, dp)
        dfs(x - 1, y, dp)
        dfs(x - 1, y + 1, dp)
        dfs(x, y - 1, dp)
        dfs(x, y + 1, dp)
        dfs(x + 1, y - 1, dp)
        dfs(x + 1, y, dp)
        dfs(x + 1, y + 1, dp)
    else:
        return


# for(int j=0;j<x;j++)
# for(int k=0;k<y;k++)

count_ponds(["#--------##-",
             "-###-----###",
             "----##---##-",
             "---------##-",
             "---------#--",
             "--#------#--",
             "-#-#-----##-",
             "#-#-#-----#-",
             "-#-#------#-",
             "--#-------#-"])
count_ponds(["#--------##-",
             "-###-----###",
             "----##---##-",
             "---------##-",
             "---------#--",
             "--#------#--",
             "-#-#-----##-",
             "#-#-#-----#-",
             "-#-#------#-",
             "--#########-"])

count_ponds(["#---------",
             "---#-----#",
             "----##----",
             ])

# ToDo
