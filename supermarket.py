import numpy


def supermarket(Items):
    vis = numpy.zeros(shape=(1000, 1))

    add = 0
    n = len(Items)
    data = sorted(Items, key=lambda x: x[0])
    data = data[::-1]

    for i in range(n):
        for j in range(data[i][1], 0, -1):
            if vis[j] == 0:
                add = add + data[i][0]
                vis[j] = 1
                break

    return add


if supermarket([(20, 1), (2, 1), (10, 3), (100, 2), (8, 2), (5, 20), (50, 10)]) == 185:
    print('passed')
else:
    print('failed')
    # ToDo



